﻿using CefSharp;
using CefSharp.OffScreen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebBrowser
{
    class Program
    {
        private static ChromiumWebBrowser browser;
        static void Main(string[] args)
        {
            const string testUrl = "https://avsigncloud.com/qa/Menuboard1.html?data=W3siSXRlbXMiOlt7Ikl0ZW1OYW1lIjoiSXRlbSBOYW1lIiwiSXRlbURlc2NyaXB0aW9uIjoiSXRlbSBEZXNjcmlwdGlvbiIsIkl0ZW1QcmljZSI6IiQxMC4wMCJ9LHsiSXRlbU5hbWUiOiJJdGVtIE5hbWUiLCJJdGVtRGVzY3JpcHRpb24iOiJJdGVtIERlc2NyaXB0aW9uIiwiSXRlbVByaWNlIjoiJDEwLjAwIn0seyJJdGVtTmFtZSI6Ikl0ZW0gTmFtZSIsIkl0ZW1EZXNjcmlwdGlvbiI6Ikl0ZW0gRGVzY3JpcHRpb24iLCJJdGVtUHJpY2UiOiIkMTAuMDAifSx7Ikl0ZW1OYW1lIjoiTWVudSBJdGVtIDEiLCJJdGVtRGVzY3JpcHRpb24iOiJNZW51IGl0ZW0gMSBkZXNjcmlwdGlvbiIsIkl0ZW1QcmljZSI6IjEyLjAwIn1dLCJNZW51TmFtZSI6Ik1lbnUgMiIsIkJhY2tncm91ZENvbG9yIjpudWxsLCJCYWNrZ3JvdW5kSW1hZ2UiOm51bGwsIkJhY2tncm91bmRVcmwiOm51bGwsIk1lbnVGb250U2l6ZSI6IjY1IiwiTWVudUZvbnRGYW1pbHkiOiJUaW1lcyBOZXcgUm9tYW4iLCJNZW51Rm9udENvbG9yIjoiIzAwMDAwMCIsIk1lbnVGb250VVJMIjpudWxsLCJJdGVtRm9udFNpemUiOiIyNiIsIkl0ZW1Gb250RmFtaWx5IjoiVGltZXMgTmV3IFJvbWFuIiwiSXRlbUZvbnRDb2xvciI6IiMwMDAwMDAiLCJJdGVtRm9udFVSTCI6bnVsbCwiSXRlbURlc2NGb250U2l6ZSI6IjE1IiwiSXRlbURlc2NGb250RmFtaWx5IjoiVGltZXMgTmV3IFJvbWFuIiwiSXRlbURlc2NGb250Q29sb3IiOiIjMDAwMDAwIiwiSXRlbURlc2NGb250VVJMIjpudWxsLCJJdGVtUHJpY2VGb250U2l6ZSI6IjI2IiwiSXRlbVByaWNlRm9udEZhbWlseSI6IlRpbWVzIE5ldyBSb21hbiIsIkl0ZW1QcmljZUZvbnRDb2xvciI6IiMwMDAwMDAiLCJJdGVtUHJpY2VGb250VVJMIjpudWxsfSx7Ikl0ZW1zIjpbeyJJdGVtTmFtZSI6Ikl0ZW0gTmFtZSIsIkl0ZW1EZXNjcmlwdGlvbiI6Ikl0ZW0gRGVzY3JpcHRpb24iLCJJdGVtUHJpY2UiOiIkMTAuMDAifV0sIk1lbnVOYW1lIjoiTWVudSAzIiwiQmFja2dyb3VkQ29sb3IiOm51bGwsIkJhY2tncm91bmRJbWFnZSI6bnVsbCwiQmFja2dyb3VuZFVybCI6bnVsbCwiTWVudUZvbnRTaXplIjoiNjUiLCJNZW51Rm9udEZhbWlseSI6IlRpbWVzIE5ldyBSb21hbiIsIk1lbnVGb250Q29sb3IiOiIjMDAwMDAwIiwiTWVudUZvbnRVUkwiOm51bGwsIkl0ZW1Gb250U2l6ZSI6IjI2IiwiSXRlbUZvbnRGYW1pbHkiOiJUaW1lcyBOZXcgUm9tYW4iLCJJdGVtRm9udENvbG9yIjoiIzAwMDAwMCIsIkl0ZW1Gb250VVJMIjpudWxsLCJJdGVtRGVzY0ZvbnRTaXplIjoiMTMiLCJJdGVtRGVzY0ZvbnRGYW1pbHkiOiJUaW1lcyBOZXcgUm9tYW4iLCJJdGVtRGVzY0ZvbnRDb2xvciI6IiMwMDAwMDAiLCJJdGVtRGVzY0ZvbnRVUkwiOm51bGwsIkl0ZW1QcmljZUZvbnRTaXplIjoiMjYiLCJJdGVtUHJpY2VGb250RmFtaWx5IjoiVGltZXMgTmV3IFJvbWFuIiwiSXRlbVByaWNlRm9udENvbG9yIjoiIzAwMDAwMCIsIkl0ZW1QcmljZUZvbnRVUkwiOm51bGx9LHsiSXRlbXMiOlt7Ikl0ZW1OYW1lIjoiSXRlbSBOYW1lIiwiSXRlbURlc2NyaXB0aW9uIjoiSXRlbSBEZXNjcmlwdGlvbiIsIkl0ZW1QcmljZSI6IiQxMC4wMCJ9XSwiTWVudU5hbWUiOiJNZW51IDEiLCJCYWNrZ3JvdWRDb2xvciI6bnVsbCwiQmFja2dyb3VuZEltYWdlIjpudWxsLCJCYWNrZ3JvdW5kVXJsIjpudWxsLCJNZW51Rm9udFNpemUiOiI2NSIsIk1lbnVGb250RmFtaWx5IjoiVGltZXMgTmV3IFJvbWFuIiwiTWVudUZvbnRDb2xvciI6IiMwMDAwMDAiLCJNZW51Rm9udFVSTCI6bnVsbCwiSXRlbUZvbnRTaXplIjoiMjYiLCJJdGVtRm9udEZhbWlseSI6IlRpbWVzIE5ldyBSb21hbiIsIkl0ZW1Gb250Q29sb3IiOiIjMDAwMDAwIiwiSXRlbUZvbnRVUkwiOm51bGwsIkl0ZW1EZXNjRm9udFNpemUiOiIxMyIsIkl0ZW1EZXNjRm9udEZhbWlseSI6IlRpbWVzIE5ldyBSb21hbiIsIkl0ZW1EZXNjRm9udENvbG9yIjoiIzAwMDAwMCIsIkl0ZW1EZXNjRm9udFVSTCI6bnVsbCwiSXRlbVByaWNlRm9udFNpemUiOiIyNiIsIkl0ZW1QcmljZUZvbnRGYW1pbHkiOiJUaW1lcyBOZXcgUm9tYW4iLCJJdGVtUHJpY2VGb250Q29sb3IiOiIjMDAwMDAwIiwiSXRlbVByaWNlRm9udFVSTCI6bnVsbH1d";

            Console.WriteLine("This example application will load {0}, take a screenshot, and save it to your desktop.", testUrl);
            Console.WriteLine("You may see Chromium debugging output, please wait...");
            Console.WriteLine();

            //Monitor parent process exit and close subprocesses if parent process exits first
            //This will at some point in the future becomes the default
            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;

            var settings = new CefSettings()
            {
                //By default CefSharp will use an in-memory cache, you need to specify a Cache Folder to persist data
                CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
            };

            //Perform dependency check to make sure all relevant resources are in our output directory.
            Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null);

            // Create the offscreen Chromium browser.
            browser = new ChromiumWebBrowser(testUrl);

            //browser.Size = new System.Drawing.Size(1920, 648);

            // An event that is fired when the first page is finished loading.
            // This returns to us from another thread.
            browser.LoadingStateChanged += BrowserLoadingStateChanged;

            // We have to wait for something, otherwise the process will exit too soon.
            Console.ReadKey();

            // Clean up Chromium objects.  You need to call this in your application otherwise
            // you will get a crash when closing.
            Cef.Shutdown();

        }


        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            // Check to see if loading is complete - this event is called twice, one when loading starts
            // second time when it's finished
            // (rather than an iframe within the main frame).
            if (!e.IsLoading)
            {
                // Remove the load event handler, because we only want one snapshot of the initial page.
                browser.LoadingStateChanged -= BrowserLoadingStateChanged;

                var scriptTask = browser.EvaluateScriptAsync("document.getElementById('lst-ib').value = 'CefSharp Was Here!'");

                scriptTask.ContinueWith(t =>
                {
                    //Give the browser a little time to render
                    Thread.Sleep(500);
                    // Wait for the screenshot to be taken.
                    var task = browser.ScreenshotAsync();
                    task.ContinueWith(x =>
                    {
                        // Make a file to save it to (e.g. C:\Users\jan\Desktop\CefSharp screenshot.png)
                        var screenshotPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "CefSharp screenshot.png");

                        Console.WriteLine();
                        Console.WriteLine("Screenshot ready. Saving to {0}", screenshotPath);

                        // Save the Bitmap to the path.
                        // The image type is auto-detected via the ".png" extension.
                        task.Result.Save(screenshotPath);

                        // We no longer need the Bitmap.
                        // Dispose it to avoid keeping the memory alive.  Especially important in 32-bit applications.
                        task.Result.Dispose();

                        Console.WriteLine("Screenshot saved.  Launching your default image viewer...");

                        // Tell Windows to launch the saved image.
                        Process.Start(new ProcessStartInfo(screenshotPath)
                        {
                            // UseShellExecute is false by default on .NET Core.
                            UseShellExecute = true
                        });

                        Console.WriteLine("Image viewer launched.  Press any key to exit.");
                    }, TaskScheduler.Default);
                });
            }
        }

    }
}
    
